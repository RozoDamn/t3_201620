package interfaz;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.Iterator;

import parqueadero.Carro;
import parqueadero.Central;

public class Main 
{
	private BufferedReader br;
	private Central central;

	/**
	 * Clase principal de la aplicaci�nn, incializa el mundo.
	 * @throws Exception
	 */
	public Main() throws Exception
	{

		br = new BufferedReader(new InputStreamReader(System.in));
		System.out.println("** PLATAFORMA CITY PARKING S.A.S **");
		System.out.println();
		central = new Central ();
		menuInicial();
	}

	/**
	 * Menú principal de la aplicación
	 * @throws Exception
	 */
	public void menuInicial() throws Exception
	{
		String mensaje = "Men� principal: \n"
				+ "1. Registrar Cliente \n"
				+ "2. Parquear Siguente Carro En Cola \n"
				+ "3. Atender Cliente Que Sale \n"
				+ "4. Ver Estado Parqueaderos \n"
				+ "5. Ver Carros En Cola \n"
				+ "6. Salir \n\n"
				+ "Opcion: ";

		boolean terminar = false;
		while ( !terminar )
		{
			System.out.print(mensaje);
			int op1 = Integer.parseInt(br.readLine());
			while (op1>6 || op1<1)
			{
				System.out.println("\nERROR: Ingrese una opci�n valida\n");
				System.out.print(mensaje);
				op1 = Integer.parseInt(br.readLine());
			}

			switch(op1)
			{
			case 1: registrarCliente(); break;
			case 2: parquearCarro(); break;
			case 3: salidaCliente(); break;
			case 4: verEstadoParquederos(); break;
			case 5: verCarrosEnCola(); break;
			case 6: terminar = true; System.out.println("\n Terminacion de Servicios. Hasta pronto."); break;
			}
		}

	}

	/**
	 * M�todo para registrar a un nuevo cliente
	 * @throws Exception
	 */
	public void registrarCliente () throws Exception
	{
		try {
			System.out.println("** REGISTRAR CLIENTE **");
			System.out.println("Por favor introduzca el nombre del cliente: ");
			String nombre = br.readLine();
			System.out.println("Digite la matricula del carro: ");
			String matricula = br.readLine();
			System.out.println("Por �ltimo, introduzca el color del vehiculo: ");
			String color = br.readLine();
			central.registrarCliente(color, matricula, nombre);
		}
		catch (Exception e )
		{
			System.out.println(e.getMessage());
		}
	}
	/**
	 * M�todo para parquear un carro que se encuentra en la cola
	 * @throws Exception
	 */
	public void parquearCarro() throws Exception
	{
		System.out.println("** PARQUEAR CARRO EN COLA **");
		try
		{
			System.out.println(central.parquearCarroEnCola());
		}
		catch (Exception e)
		{
			System.out.println(e.getMessage()); 
		}
	}
	/**
	 * M�todo para sacar un carro del parqueadero
	 * @throws Exception
	 */
	public void salidaCliente () throws Exception
	{
		System.out.println("** REGISTRAR SALIDA CLIENTE **");
		try
		{
			System.out.println("Introduzca la matricula del carro a sacar: ");
			String matricula = br.readLine();
			System.out.println("El valor a cancelar es de: $" + central.atenderClienteSale(matricula) + " Pesos");
		}
		catch(Exception e)
		{
			System.out.println(e.getMessage());
		}

	}
	/**
	 * M�todo que permite visualizar graficaente el estado de los parqueaderos
	 * @throws Exception
	 */
	public void verEstadoParquederos() throws Exception
	{
		System.out.println("** ESTADO PARQUEADEROS **");
		try
		{
			Iterator<Carro> itr = central.darPila1().iterator();
			while (itr.hasNext())
			{
				System.out.print("O");
				Carro temp = itr.next();
			}
			Iterator<Carro> itr2 = central.darPila2().iterator();
			while (itr.hasNext())
			{
				System.out.print("O");
				Carro temp = itr2.next();
			}
			Iterator<Carro> itr3 = central.darPila3().iterator();
			while (itr.hasNext())
			{
				System.out.print("O");
				Carro temp = itr3.next();
			}
			Iterator<Carro> itr4 = central.darPila4().iterator();
			while (itr.hasNext())
			{
				System.out.print("O");
				Carro temp = itr4.next();
			}
			Iterator<Carro> itr5 = central.darPila5().iterator();
			while (itr.hasNext())
			{
				System.out.print("O");
				Carro temp = itr5.next();
			}
			Iterator<Carro> itr6 = central.darPila6().iterator();
			while (itr.hasNext())
			{
				System.out.print("O");
				Carro temp = itr6.next();
			}
			Iterator<Carro> itr7 = central.darPila7().iterator();
			while (itr.hasNext())
			{
				System.out.print("O");
				Carro temp = itr7.next();
			}
			Iterator<Carro> itr8 = central.darPila8().iterator();
			while (itr.hasNext())
			{
				System.out.print("O");
				Carro temp = itr8.next();
			}
		}
		catch(Exception e)
		{
			System.out.println(e.getMessage());
		}
	}

	/**
	 * M�todo que permite visualizar graficaente el estado de la cola de carros pendientes por parquear
	 * @throws Exception
	 */
	public void verCarrosEnCola () throws Exception
	{
		System.out.println("** ESTADO COLA **");
		try{
			Iterator itr = central.darFila().iterator();
			while (itr.hasNext()){
				System.out.println("O");
				Carro temp = (Carro)itr.next();
			}
		}
		catch(Exception e)
		{
			System.out.println(e.getMessage()); 
		}
	}

	/**
	 * Main...
	 * @param args
	 */
	public static void main(String[] args) {
		try {
			new Main();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

}
