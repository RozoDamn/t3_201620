package parqueadero;

import java.util.Iterator;

public class Fila <T> implements Iterable<T>
{ 
	private Node first;
	private Node last;
	private int size; 


	private class Node
	{
		T element;
		Node next; 
	}

	public Fila()
	{
		first = null;
		last = null;
		size = 0; 
	}
	
	public T giveFirst()
	{
		return first.element;
	}

	public boolean estaVacio()
	{
		return first == null;
	}

	public void enqueue(T objeto)
	{
		Node ultimoViejo = last;
		last = new Node();
		last.element =objeto;
		last.next = null;
		if (estaVacio())
		{
			first = last;

		}
		else 
		{
			ultimoViejo.next = last; 
		}
		size++;
	}

	public T dequeue ()
	{
		T item = first.element;
		first = first.next;
		if (estaVacio())
		{
			last = null;
		}
		return item; 
	}

	public int darTama�o()
	{
		return size;
	}



	public Iterator<T> iterator() 
	{
		return new IteradorLista();
	}

	private class IteradorLista implements Iterator<T>
	{
		private Node actual = first;

		public boolean hasNext() {
			return actual != null;
		}

		public T next() {
			if (!hasNext())
			{
				throw new UnsupportedOperationException();
			}
			T item = actual.element;
			actual = actual.next;
			return item;
		}

		public void remove() {
			throw new UnsupportedOperationException();
			// TODO Auto-generated method stub
			
		}
	}
}
