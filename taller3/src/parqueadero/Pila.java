package parqueadero;

import java.util.Iterator;





public class Pila <T>
{
	private Node primero = null;
	
	private class Node
	{
		T element; 
		Node next;
	}
	
	public Iterator<T> iterator() 
	{
		return new IteradorLista();
	}
	
	public boolean estaVacio()
	{
		return primero == null;
	}
	
	public void push(T objeto)
	{
		Node ultimoViejo = primero;
		primero = new Node();
		primero.element = objeto;
		primero.next = ultimoViejo;
	}
	
	public T pop()
	{
		T item = primero.element;
		primero = primero.next;
		return item;
	}
	
	private class IteradorLista implements Iterator<T>
	{
		private Node actual = primero;

		public boolean hasNext() {
			return actual != null;
		}

		public T next() {
			if (!hasNext())
			{
				throw new UnsupportedOperationException();
			}
			T item = actual.element;
			actual = actual.next;
			return item;
		}

		public void remove() 
		{
			throw new UnsupportedOperationException();
		}
	}
	

}
